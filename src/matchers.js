const { matcherHint, printReceived, printExpected } = require('jest-matcher-utils');
const { isString, get } = require('lodash');

module.exports = {
  toHaveLoggedVueErrors(consoleSpy) {
    const calls = get(consoleSpy, 'error.mock.calls', []);
    const loggedVueErrors = calls.some(([call]) => isString(call) && call.startsWith('[Vue warn]'));
    return {
      pass: loggedVueErrors,
      message: () =>
        loggedVueErrors
          ? 'Vue errors were logged to the console'
          : 'No Vue errors were logged to the console',
    };
  },

  // Adopted from https://github.com/testing-library/jest-dom/blob/master/src/to-have-focus.js
  toHaveFocus(element) {
    return {
      pass: element.ownerDocument.activeElement === element,
      message: () => {
        return [
          matcherHint(`${this.isNot ? '.not' : ''}.toHaveFocus`, 'element', ''),
          '',
          'Expected',
          `  ${printExpected(element)}`,
          'Received:',
          `  ${printReceived(element.ownerDocument.activeElement)}`,
        ].join('\n');
      },
    };
  },

  toHaveSpriteIcon(element, iconName) {
    if (!iconName) {
      throw new Error('toHaveSpriteIcon is missing iconName argument!');
    }

    if (!(element instanceof HTMLElement)) {
      throw new Error(`${element} is not a DOM element!`);
    }

    const iconReferences = [].slice.apply(element.querySelectorAll('svg use'));
    const matchingIcon = iconReferences.find((reference) =>
      reference.getAttribute('xlink:href').endsWith(`#${iconName}`)
    );

    const pass = Boolean(matchingIcon);

    let message;
    if (pass) {
      message = `${element.outerHTML} contains the sprite icon "${iconName}"!`;
    } else {
      message = `${element.outerHTML} does not contain the sprite icon "${iconName}"!`;

      const existingIcons = iconReferences.map((reference) => {
        const iconUrl = reference.getAttribute('xlink:href');
        return `"${iconUrl.replace(/^.+#/, '')}"`;
      });
      if (existingIcons.length > 0) {
        message += ` (only found ${existingIcons.join(',')})`;
      }
    }

    return {
      pass,
      message: () => message,
    };
  },

  toMatchInterpolatedText(received, match) {
    let clearReceived;
    let clearMatch;

    try {
      clearReceived = received.replace(/\s\s+/gm, ' ').replace(/\s\./gm, '.').trim();
    } catch (e) {
      return {
        actual: received,
        message: 'The received value is not a string',
        pass: false,
      };
    }
    try {
      clearMatch = match.replace(/%{\w+}/gm, '').trim();
    } catch (e) {
      return { message: 'The comparator value is not a string', pass: false };
    }
    const pass = clearReceived === clearMatch;
    const message = pass
      ? () => `
          \n\n
          Expected: ${this.utils.printExpected(clearReceived)}
          To not equal: ${this.utils.printReceived(clearMatch)}
          `
      : () =>
          `
        \n\n
        Expected: ${this.utils.printExpected(clearReceived)}
        To equal: ${this.utils.printReceived(clearMatch)}
        `;

    return { actual: received, message, pass };
  },
};
